<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.04.17
 * Time: 8:52
 */

/**
 * Class ConnectionSelector
 *
 * Устанавливает соединение по протоколу учитывая приоритет и доступность
 */
class ConnectionSelector
{
    private $_config;

    public function __construct()
    {
        $this->_config = include_once 'config/config.php';
    }

    private function getConnectionByPriority()
    {
        $sortItems = array();
        foreach ($this->_config as $key => $value)
        {
            $sortItems[$key] = $value['priority'];
        }

        array_multisort($sortItems, SORT_ASC, $this->_config);

        foreach ($this->_config as $connection)
        {
            if(($connection = $this->getConnectionByClass($connection['class'])) !== false)
                return $connection;
        }
    }

    private function getConnectionByClass($class)
    {
        $connection = new $class;
        if($connection instanceof \protocols\Connector)
            return $connection;

        throw new \exceptions\Exchange1cException('Класс '.$class.' не реализует интерфейс ConnectionInterface');
    }

    /**
     * @return mixed - возвращает установленное соединение
     */
    public function getConnection()
    {
        return $this->getConnectionByPriority();
    }

}