<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.04.17
 * Time: 10:36
 */

/**
 * Class CurlSoapClient
 * Расширяет SoapClient выполняя запросы через cURL
 */
class CurlSoapClient extends SoapClient
{
    /**
     * Включение, выключение кэширования wsdl файла
     * @var bool
     */
    public $wsdl_cache_enabled = true;

    /**
     * Директория хранения wsdl файла
     * @var string
     */
    public $wsdl_cache_dir = '/tmp';

    /**
     * Срок хранения wsdl файла в секундах
     * @var int
     */
    public $wsdl_cache_ttl = 86400;

    /**
     * Тип кэширования wsdl файла
     * WSDL_CACHE_NONE (0), WSDL_CACHE_DISK (1), WSDL_CACHE_MEMORY (2) или WSDL_CACHE_BOTH (3)
     * @var int
     */
    public $wsdl_cache = WSDL_CACHE_DISK;

    /**
     * Максимальное количество находящихся в оперативной памяти кэшированных файлов WSDL. Дальнейшее добавление файлов в заполненную кэш-память будет приводить к удалению из нее самых старых файлов.
     * @var int
     */
    public $wsdl_cache_limit = 5;

    private $_curl;

    public function __construct($wsdl, array $options)
    {
        ini_set("soap.wsdl_cache_enabled", $this->wsdl_cache_enabled);
        ini_set("soap.wsdl_cache_dir", $this->wsdl_cache_dir);
        ini_set("soap.wsdl_cache_ttl", $this->wsdl_cache_ttl);
        ini_set("soap.wsdl_cache", $this->wsdl_cache);
        ini_set("soap.wsdl_cache_limit", $this->wsdl_cache_limit);
        ini_set('default_socket_timeout', $options['connection_timeout']);

        $this->_curl = new CurlRequest($options);

        parent::__construct($wsdl, $options);
    }

    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        return $this->_curl->doRequest($request, $location);
    }

}