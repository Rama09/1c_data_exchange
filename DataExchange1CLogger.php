<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 25.04.17
 * Time: 11:51
 */
class DataExchange1CLogger
{
    private $_object;

    public function __construct()
    {

        try {
            $this->_object = new DataExchange1C();
        } catch (Exception $e) {
            $this->_exceptionProcessing($e);
        }
    }

    public function __call($name, $arguments)
    {
        try {
            if (isset($arguments[0]))
                return $this->_object->$name($arguments[0]);
            else
                return $this->_object->$name();

        } catch (Exception $e) {
            $this->_exceptionProcessing($e);
        }
    }

    private function _exceptionProcessing(Exception $error)
    {
        date_default_timezone_set('Europe/Moscow');
        $file_path = __DIR__ . '/log/log.txt';
        $data = date('d.m.y H:i') . " | " . get_class($error) . ' | ' . $error->getMessage() . PHP_EOL;
        file_put_contents($file_path, $data, FILE_APPEND);

        echo 'connection false';

    }


}