<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.04.17
 * Time: 11:40
 */

/**
 * Class DataExchange1C
 * Получает данные из 1C
 * Класс для использования в клиентском коде
 */
class DataExchange1C
{
    private $_connector;

    public function __construct()
    {
        $this->_connector = new ConnectionSelector();
    }

    public function getConnection()
    {
        $connection = $this->_connector->getConnection();

        return $connection->isConnected() ? $connection : false;
    }
    
}