<?php

require_once 'require.php';
ini_set('display_errors', E_ALL);

if (\SessionCache::lifetimeCalculation('history', 2 * 60)) {
    // Забираем данные из кэша
    $result =  \SessionCache::load('history');
} else {
    // Если в кэше пусто или истекло время хранения
    $exchange = new DataExchange1CLogger();
    $connection = $exchange->getConnection();

    if ($connection !== false) {
        $connection->setUserId(315);
        $result = $connection->simpleOrderHistory();
        \SessionCache::save($result, 'history');
    } else {
        // Если ошибка при подключении
        $result =  \SessionCache::load('history');
        if (empty($result)) {
            // Если нет в кэше берем из базы или наоборот
        }
    }

}




if (\SessionCache::lifetimeCalculation('credits', 2 * 60)) {
    // Забираем данные из кэша
    $result1 =  \SessionCache::load('credits');
} else {
    // Если в кэше пусто или истекло время хранения
    $exchange = new DataExchange1CLogger();
    $connection = $exchange->getConnection();

    if ($connection !== false) {
        $connection->setUserId(315);
        $result1 = $connection->getCreditsInfo();
        \SessionCache::save($result1, 'credits');
    } else {
        // Если ошибка при подключении
        $result1 =  \SessionCache::load('credits');
        if (empty($result1)) {
            // Если нет в кэше берем из базы или наоборот
        }
    }

}


echo "<pre>";
print_r($result);
echo "</pre>";

echo "<pre>";
print_r($result1);
echo "</pre>";