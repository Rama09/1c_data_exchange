<?php
namespace protocols;

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.04.17
 * Time: 11:15
 */
use interfaces\OneCInterface;

/**
 * Class SoapConnectionClass
 *
 * Подключение по протоколу SOAP
 */
class SoapConnectionClass extends Connector
{
    private $_connection;

    public function __construct()
    {
        parent::__construct();
        $this->soapOptions['options']['connection_timeout'] = $this->getTimeout();
        $this->_connection = new \CurlSoapClient($this->soapOptions['wsdl'], $this->soapOptions['options']);
    }

    /**
     * @throws \exceptions\Exchange1cException
     *
     * Устанавливает соединение по протоколу
     */
    public function isConnected()
    {
        if ($this->_connection->isOnline()->return !== true)
            throw new \exceptions\Exchange1cException('Ошибка при проверке подключения функцией isOnline');

        return true;
    }


    /**
     * @param $user_id
     * @return mixed
     *
     * Простой список начисления/списания баллов
     */
    public function simpleOrderHistory()
    {
        return $this->_connection->SimpleOrderHistory(array('id' => $this->getUserId()));
    }

    /**
     * @return mixed
     *
     * Бонусный баланс
     */
    public function getCreditsInfo()
    {
        return $this->_connection->GetCreditsInfo(array('id' => $this->getUserId()));
    }

}