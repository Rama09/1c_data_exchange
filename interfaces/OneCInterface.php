<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 24.04.17
 * Time: 9:30
 */

namespace interfaces;

/**
 * Interface OneCInterface
 * @package interfaces
 *
 * Определяет методы 1с
 */
interface OneCInterface
{
    /**
     * @param $user_id
     * @return mixed
     *
     * Простая история начисления/списания бонусных баллов
     */
    public function simpleOrderHistory();
}