<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 17.04.17
 * Time: 8:47
 */

/**
 * Class CurlRequest
 * Отправляет запрос через cURL
 * __construct принимает массив настроек с обязательными элементами login, password и url
 */
class CurlRequest
{
    private $_auth_login;
    private $_auth_pass;
    private $_timeout;
    private $_location;

    private $_request_body;
    private $_ch;
    private $_curl_options;
    private $_response_xml;

    /**
     * CurlRequest constructor.
     * @param array $options = array('login' => '', 'password' => '', 'url' => '');
     * @throws \exceptions\Exchange1cException
     */
    public function __construct(array $options)
    {
        if (!isset($options['login']) || !isset($options['password']))
            throw new \exceptions\Exchange1cException('Не установлены логин/пароль для данного соединения');

        $this->_auth_login = $options['login'];
        $this->_auth_pass = $options['password'];
        $this->_timeout = $options['connection_timeout'];
    }

    /**
     * @param $request_body - тело запроса в формате пригодном для сервера-получателя
     * @param $location - url для отправки запроса
     * @return mixed - результат запроса
     * @throws \exceptions\Exchange1cException
     *
     * Выполнение запроса
     */
    public function doRequest($request_body, $location)
    {
        $this->_requestPreparation($request_body, $location);

        $this->_response_xml = curl_exec($this->_ch);

        $this->_requestCompletion();

        return $this->_response_xml;
    }

    /**
     * @param $request_body - тело запроса в формате пригодном для сервера-получателя
     * @param $location - url для отправки запроса
     * @return bool
     * @throws \exceptions\Exchange1cException
     *
     * Подготовка запроса: инициализация, установка настроек cURL
     */
    private function _requestPreparation($request_body, $location)
    {
        $this->_request_body = $request_body;
        $this->_location = $location;

        $this->_ch = curl_init();

        if ($this->curlSetOptions() === false)
            throw new \exceptions\Exchange1cException(curl_error($this->_ch));

        return true;
    }

    /**
     * @return bool
     * @throws \exceptions\Exchange1cException
     *
     * Завершение запроса: проверка получения ответа, закрытие соединения
     */
    private function _requestCompletion()
    {
        if ($this->_response_xml === false)
            throw new \exceptions\Exchange1cException(curl_error($this->_ch));

        curl_close($this->_ch);

        return true;
    }

    /**
     * @return bool - значение функции curl_setopt_array()
     *
     * Устанавливает настройки cURL
     */
    private function curlSetOptions()
    {
        $this->_curl_options = array(
            CURLOPT_HTTPAUTH        => CURLAUTH_BASIC,
            CURLOPT_USERPWD         => $this->_auth_login . ":" . $this->_auth_pass,
            CURLOPT_URL             => $this->_location,
            CURLOPT_CONNECTTIMEOUT  => $this->_timeout,       // timeout on connect
            CURLOPT_TIMEOUT         => $this->_timeout,       // timeout on response
            CURLOPT_RETURNTRANSFER  => true,                     // return web page
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_SSL_VERIFYHOST  => false,                    // don't verify ssl
            CURLOPT_POST            => true,                     // i am sending post data
            CURLOPT_POSTFIELDS      => $this->_request_body,     // this are my post vars
        );

        return curl_setopt_array($this->_ch, $this->_curl_options);
    }

}