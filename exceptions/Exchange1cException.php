<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 11.04.17
 * Time: 11:57
 */

namespace exceptions;

/**
 * Class Exchange1cException
 * @package exceptions
 * Класс для явного определения принадлежности исключения данному сервису
 */
class Exchange1cException extends \Exception
{

}