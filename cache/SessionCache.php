<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 09.06.17
 * Time: 8:49
 */
class SessionCache
{

    public static function lifetimeCalculation($session_name, $life_time)
    {
        if (session_status() !== 2)
            session_start();

        if (isset($_SESSION[$session_name]['start_time']) && isset($_SESSION[$session_name]['value'])) {
            $session_time = $_SESSION[$session_name]['start_time'];
            $finish_time = $session_time + $life_time;

            if ($finish_time > time()) {
                return true;
            }
        }

        return false;
    }

    public static function save($value, $session_name)
    {
        echo '**********запрос';
        $_SESSION[$session_name] = array(
            'start_time' => time(),
            'value' => $value,
        );
    }

    public static function load($session_name)
    {
        echo '*********кеш';
        return $_SESSION[$session_name]['value'];
    }

    public static function delete($session_name)
    {
        unset($_SESSION[$session_name]);
    }
}