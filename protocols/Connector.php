<?php
namespace protocols;
/**
 * Created by PhpStorm.
 * User: eugene
 * Date: 24.04.17
 * Time: 14:42
 */
abstract class Connector
{
    private $timeout = 10;
    private $_user_id;

    public function __construct()
    {
        $this->configure();
    }

    /**
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

    private function configure()
    {
        $options = $this->getOptions();
        if($options === false) return;

        foreach ($options as $attribute => $value)
        {
            $this->$attribute = $value;
        }
    }

    private function getOptions()
    {
        $config = include dirname(__DIR__).'/config/config.php';
        foreach ($config as $key => $value)
        {
            if($value['class'] === get_called_class())
                return $value['options'];
            else continue;
        }
        return false;
    }

    abstract public function isConnected();

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->_user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->_user_id = (int)$user_id;
    }

    protected function cacheRun()
    {
        $debug = debug_backtrace();
        var_dump($debug[1]['function']);
    }
}